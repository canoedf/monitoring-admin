# README #

### Screenshots ###

* Monitoring Admin tool
![Monitoring-Admin.png](https://bitbucket.org/repo/K6LEpn/images/4004607015-Monitoring-Admin.png)

* Monitoring Admin Events
![Events.png](https://bitbucket.org/repo/K6LEpn/images/3446799247-Events.png)

* Monitoring Admin Performance
![performance.png](https://bitbucket.org/repo/K6LEpn/images/565880376-performance.png)


This MONITORING-ADMIN operations tool, meant for internal workstation usage, was originally conceived and developed
in response to the awful monitoring tool user interfaces I had to endure.

This version assumes that HP SiteScope has been configured and is sending monitoring results to the SiteScope database detailed below.

The original effort was confined to a status display that I called SMARTS showing error/warning detail provided by the monitoring alert function. Now I have included a performance display and some tools.

The tool does not rely on any UI "framework". Current frameworks do not provide enough support for all the integrations that may grow with this tool.
Dan

### What is this repository for? ###
* Monitoring Admin tool
* 0.1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###
* Search Google

### How do I get set up? ###
* You need to set the database credentials in two places:

```
#!php

/php/dbconnect.php
/php/hostcount.php
```
The hostnames need to be set for the switch structure here:

```
#!php

/php/runtool.php
```
* Dependencies - all included in the source

```
#!php

* http://www.rainforestnet.com/datetimepicker/datetimepicker.htm
* Copyright (c) 2010-2014, Teng-Yong Ng . All rights reserved.
* Drop Down Tabs Menu script- © Dynamic Drive (http://www.dynamicdrive.com)
* D3 Javascript project: http://d3js.org/
* and the defunct Eyesis project
* @author Micheal Frank
* @copyright 2008 Eyesis @version v1.0.1 11/26/2008 9:41:46 AM
* @license http://www.eyesis.ca/license.txt BSD License
```

* Database configuration - create the database, tables and columns 

```
#!php

Database 'sitescope'

Tables/Columns:

SiteScopeHosts
groupName	varchar(255) and index

SiteScopeLog *all fields varchar(255) - this is HP SiteScope default
datex
serverName
class
sample
category
groupName
monitorName
status		
monitorID
value1
value2
value3
value4
value5
value6
value7
value8
value9
value10
sourceIP
sourceID

SiteScopeLogalert *all fields varchar(255)
time
category
groupName
monitorName
status

SiteScopeLogalertHistory *all fields varchar(255)
time
category
groupName
monitorName
status

SiteScopeLogmetricJournal *all fields varchar(255)
groupName
monitor
tm
metric

smart_ack
Time	 varchar(25)
Host	 varchar(25)
Monitor	 varchar(25)
Status	 varchar(128)
Event	 varchar(25)
fielda	 varchar(50)
incident varchar(75)
Name	 varchar(25)
Alert	 varchar(25)
Source	 varchar(25)
AckEnd	 varchar(25)

smart_alerts
Time	 varchar(25)
Host	 varchar(25)
Monitor	 varchar(25)
Status	 varchar(128)
Event	 varchar(25)
fielda	 varchar(50)
incident varchar(75)
Name	 varchar(25)
Alert	 varchar(25)
Source	 varchar(25)

smart_grid
Time	 varchar(25)
Host	 varchar(25)
Monitor	 varchar(25)
Status	 varchar(128)
Event	 varchar(25)
fielda	 varchar(50)
incident varchar(75)
Name	 varchar(25)
Alert	 varchar(25)
Source	 varchar(25)

smart_history
Time	 varchar(25)
Host	 varchar(25)
Monitor	 varchar(25)
Status	 varchar(128)
Event	 varchar(25)
fielda	 varchar(50)
incident varchar(75)
Name	 varchar(25)
Alert	 varchar(25)
Source	 varchar(25)
```