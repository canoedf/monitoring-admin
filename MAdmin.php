<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" -->
<head>
<!--
Name: Monitoring Admin
Date created: 2015-Jan-02 11:30
Creator: Dan Fischer
Copyright (c) 2015 Dan Fischer
FileName: MAdmin.php
Version: 0.1.0
-->
<meta name="viewport" content="width=device-width">
<script type="text/javascript" src="js/dropdowntabs.js">
/**************************************************************************
* Drop Down Tabs Menu script- © Dynamic Drive (http://www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***************************************************************************/
</script>
<link rel="stylesheet" type="text/css" href="css/MAmenu.css" />

<!-- php updates the SiteScopeHosts table here  -->
<?php
include('php/hostcount.php');
?>
</head>
<body>
<div id="header">
<style>position:fixed;</style><!-- testing fixed position here  -->
	<div id="colortab" class="ddcolortabs">
		<ul>
		<li><a ><span>Monitoring-Admin</span></a></li>
		<li><a href="smart2.php" ><span>Events</span></a></li>
		<li><a href="metrics.php" ><span>Performance</span></a></li>
		<li><a href="tools.php" ><span>Tools</span></a></li>
		<li><a href="about.php" ><span>About</span></a></li>
		<li><a rel="dropmenu1_a"><span>Menu</span></a></li>
		</ul>
	</div>
</div>
<div class="ddcolortabsline">&nbsp;</div>
	<div id="body">
		<div id="dropmenu1_a" class="dropmenudiv_a">
			<a href="stub.php" ><span>Settings</span></a>
			<a href="stub.php" ><span>Audit</span></a>
			<a href="stub.php" ><span>Map</span></a>
		</div>
	</div>
</div>
<div class="somecontent"><!-- just showing logic flow graphic here - maybe replace with a network topology graphic  -->
	<center>
		<P><IMG src="dataflow.jpg" border="0">
	</center>
</div>
<div id="myfooter">
	<p><strong>Footer</strong> (always at the bottom). "Good design is as little design as possible."</p>
</div>
<script type="text/javascript">tabdropdown.init("colortab", 0)</script>
</body>
</html>