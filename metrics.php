<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" -->
<head>
<!--
Name: Monitoring Admin
Date created: 2015-Jan-02 11:30
Creator: Dan Fischer
Copyright (c) 2015 Dan Fischer
FileName: metrics.php
Version: 0.1.0
-->
<meta name="viewport" content="width=device-width">
<script type="text/javascript" src="js/d3.min.js" charset="utf-8"></script>
<script src="js/datetimepicker_css.js">
/**************************************************************************
*Copyright (c) 2010-2014, Teng-Yong Ng . All rights reserved.
*http://www.rainforestnet.com/datetimepicker/datetimepicker.htm
***************************************************************************/
</script>
<script type="text/javascript" src="js/dropdowntabs.js">
</script>
<link rel="stylesheet" type="text/css" href="css/d3Test.css" />
<link rel="stylesheet" type="text/css" href="css/MAmenu3.css" />
<script type="text/javascript" src="js/MAdmin.js" ></script>
</head>
<body>
<div id="header">
	<div id="colortab" class="ddcolortabs">
		<ul>
		<li><a ><span>Monitoring-Admin</span></a></li>
		<li><a href="MAdmin.php" ><span>Back</span></a></li>
		</ul>
	</div>
</div>
<div class="ddcolortabsline">&nbsp;</div>
	<div id="body">
	<div class="somecontent">
			<fieldset> 
				<legend>Graph settings</legend> 
				<FORM NAME="myform" ACTION="" METHOD="GET"><BR>
				<?php
				
				/* load database credentials and connect */
				include('php/dbconnect.php');
				$server = mysql_connect($dbhost, $dbuser, $dbpass);
				$connection = mysql_select_db($database, $server);
				$myquery = "SELECT groupName FROM `SiteScopeHosts` ORDER BY groupName";
				$query = mysql_query($myquery);
				if ( !$query ) { echo mysql_error(); die; }
				$data = array();
				$rowcount = mysql_num_rows($query);
				for ($x = 0; $x < mysql_num_rows($query); $x++) {$data[] = mysql_fetch_assoc($query); }
				
				/* php builds the Monitor Group dropdown from database here */
				echo 'Monitor Group: <select id="value1">';
				for ($x = 0; $x < $rowcount; $x++) {
					echo '<option value="'.$data[$x][groupName].'">'.$data[$x][groupName];
				}
				echo '</select>';
				mysql_close($server);
				?>
				&nbsp;&nbsp;&nbsp;
				Start: <input type="Text" id="value2" maxlength="25" size="25"/><img src="images/cal.gif" onclick="javascript:NewCssCal('value2','yyyyMMdd','arrow',true,'24')" style="cursor:pointer"/>&nbsp;&nbsp;&nbsp;
				End: <input type="Text" id="value3" maxlength="25" size="25"/><img src="images/cal.gif" onclick="javascript:NewCssCal('value3','yyyyMMdd','arrow',true,'24')" style="cursor:pointer"/>&nbsp;&nbsp;&nbsp;
				<INPUT TYPE="button" NAME="button" Value="Sure" onClick="selectMetric(this.form)">
				</FORM>
			</fieldset>	
		</div>
	</div>
</div>
<div id="myfooter">
	<p>
		<strong>Footer</strong> (always at the bottom). "Good design is as little design as possible."
	</p>
</div>
<script type="text/javascript">tabdropdown.init("colortab", 0)</script>
</body>
</html>