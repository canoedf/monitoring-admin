<?php
#Name: Monitoring Admin
#Date created: 2015-Jan-02 11:30
#Creator: Dan Fischer
#Copyright (c) 2015 Dan Fischer
#FileName: dbconnect.php
#Version: 0.1.0

$dbhost = 'hostname';
$dbuser = 'root';
$dbpass = 'password';
$database = 'database';
?>