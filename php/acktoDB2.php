<?php
#Name: Monitoring Admin
#Date created: 2015-Jan-02 11:30
#Creator: Dan Fischer
#Copyright (c) 2015 Dan Fischer
#FileName: acktoDB2.php
#Version: 0.1.0

# load database credentials
include('../php/dbconnect.php');
$Time = $_POST['Time'];
$Host = $_POST['Host'];
$Monitor = $_POST['Monitor'];
$Status = $_POST['Status'];
$Event = $_POST['Event'];
$fielda = $_POST['fielda'];
$incident = $_POST['incident'];
$Name = $_POST['Name'];
$Alert = $_POST['Alert'];
$Source = $_POST['Source'];
$Who = $_POST['Who'];
$AckEnd = $_POST['AckEnd'];

# Log some details
$tracefile=fopen("/tmp/JSONtoDB.log","a");
echo fputs($tracefile,$Time . "\n");

# update the smart_ack table here
$conn = mysql_connect($dbhost, $dbuser, $dbpass);
if(! $conn )
{
  echo fputs($tracefile,date(DATE_W3C) . " Failed to connect: " . $dbhost . "\n");
  fclose($tracefile);
  die('Could not connect: ' . $dbhost . "\n");
}
echo fputs($tracefile,date(DATE_W3C) . " Connected to " . $dbhost . "\n");
$sql="INSERT INTO smart_ack(Time, Host, Monitor, Status, Event, fielda, incident, Name, Alert, Source, AckEnd)" .
 "VALUES('$Time', '$Host', '$Monitor', '$Status', '$Event', '$fielda', '$incident', '$Name', '$Alert', '$Source','$AckEnd')";
$retval = mysql_select_db($database);
$retval = mysql_query( $sql, $conn );
if(! $retval )
{
  echo fputs($tracefile,date(DATE_W3C) . " Failed to ACK successfully" . "\n");
  fclose($tracefile);
  mysql_close($conn);
  die('Could not enter data: ' . "\n");
}
echo fputs($tracefile,date(DATE_W3C) . " ACK successfully" . "\n");

// a trigger will delete the record from smart_grid when smart_ack is written
# ----------------------------------------------------------------------------------------
echo fputs($tracefile,"\n" ."\n");
fclose($tracefile);
mysql_close($conn);

# clear the screen with a little javascript
echo "<script type='text/javascript'>\n";
echo "document.body.innerHTML = ''";
echo "</script>";

# back to the initial screen which will refresh with a little javascript 
echo "<script type='text/javascript'>\n";
echo "window.location.href = 'http://web.wmvtb.net/D4/smart2.php'";
echo "</script>";
?>
 