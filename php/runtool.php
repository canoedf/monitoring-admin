<?php
#Name: Monitoring Admin
#Date created: 2015-Jan-02 11:30
#Creator: Dan Fischer
#Copyright (c) 2015 Dan Fischer
#FileName: runtool.php
#Version: 0.1.0

# Get parameters from the tools.php/MAdmin.js caller and generate results
$param1 = $_POST['host'];
$param2 = $_POST['tool'];

#the host1,2,3,4 strings are just place holders for the actual host names - perhaps this would be better handled by using
# a database query and building the "switch" structure from it 
# the hostn == fqdn - setting manually here for testing - in the future replace with a database query to pull fqdn's
switch ($param1) {
    case "host1":
        $host = "host1";
        break;
    case "host2":
        $host = "host2";
        break;
    case "host3":
        $host = "host3";
        break;
	case "host4":
        $host = "host4";
        break;
    default:
        $host = $param1;
}
switch ($param2) {
    case "ping":
		$tool = "ping";
		$result = exec("ping -c 1 ".$host);
		if ($result == 0){
			echo "<BR>"."ping succeeded for host: ".$host."<BR>".$result;
		}else{
			echo "<BR>"."ping failed"."  ".$result;
		}
        break;
    case "traceroute":
		$tool = "traceroute";
		$result = exec("traceroute ".$host. "> results.txt");
		$result = file('results.txt');
		end($result);
		$last_element = key($result);
		reset($result);
		echo "<BR>";
		for ($findex=0; $findex <= $last_element; $findex++) {
			echo $result[$findex]."<BR>";
		}
        break;
    case "dns":
		$result = dns_get_record($host);
		end($result);
		$last_element = key($result);	//get the last index for the array
		reset($result);
		echo "<BR>";
		for ($findex=0; $findex <= $last_element; $findex++) {
			echo "[host]".$result[$findex][host]."<BR>";
			echo "[ttl]".$result[$findex][ttl]."<BR>";
			echo "[type]".$result[$findex][type]."<BR>";
			if ($result[$findex][target] != ''){echo "[target]".$result[$findex][target]."<BR>";}
			if ($result[$findex][ip] != ''){	echo "[ip]".$result[$findex][ip]."<BR>";}
			if ($result[$findex][mname] != ''){	echo "[mname]".$result[$findex][mname]."<BR>";}
			if ($result[$findex][rname] != ''){	echo "[mname]".$result[$findex][rname]."<BR>";}
			echo "<BR>";
		}
        break;
}
?>