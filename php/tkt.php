<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<?php
#Name: Monitoring Admin
#Date created: 2015-Jan-02 11:30
#Creator: Dan Fischer
#Copyright (c) 2015 Dan Fischer
#FileName: tkt.php
#Version: 0.1.0

# get the smart2.php callers data then display - this is a stub for ticket creation
$Time = $_POST['Time'];
$Host = $_POST['Host'];
$Monitor = $_POST['Monitor'];
$Status = $_POST['Status'];
$Event = $_POST['Event'];
$fielda = $_POST['fielda'];
$incident = $_POST['incident'];
$Name = $_POST['Name'];
$Alert = $_POST['Alert'];
$Source = $_POST['Source'];
$Who = $_POST['Who'];
echo "<br><br>"; 
?>
<style>
body {
    margin:5px;
    color:#666;
    font-family: Tahoma, Arial, Helvetica, Tahoma, serif;
    font-size:18px;
    overflow-x:hidden;
}
</style>
<?php
echo 'Do a create ticket function here ?' . "<br>";
echo 'Maybe populate a ticket field in the table for display' . "<br>";
echo 'Consider logging the ticket function in a new admin table with datetime stamp and who did it' . "<br>";
echo 'Then return by calling the original page' . "<br>";
echo 'Use this data from the grid - values sent from datagrid:' . "<br>";
echo "------------------------------------" . "<br>";
echo 'Time:  ' .$Time . "<br>";
echo 'Host:  ' .$Host . "<br>";
echo 'Monitor:  ' .$Monitor . "<br>";
echo 'Status:  ' .$Status . "<br>";
echo 'Event:  ' .$Event . "<br>";
echo 'User IP:  ' . $Who . "<br>";
echo "fielda: " . $fielda . "<br>";
echo "incident: " . $incident . "<br>";
echo "Name: " . $Name . "<br>";
echo "Alert: " . $Alert . "<br>";
echo "Source: " . $Source . "<br>";
echo "Who: " . $Who . "<br>";
?>
</html>