<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<!-- Get parameters from smart2.php caller and generate results  -->
<?php
#Name: Monitoring Admin
#Date created: 2015-Jan-02 11:30
#Creator: Dan Fischer
#Copyright (c) 2015 Dan Fischer
#FileName: acktest3.php
#Version: 0.1.0

$Time = $_POST['Time'];
$Host = $_POST['Host'];
$Monitor = $_POST['Monitor'];
$Status = $_POST['Status'];
$Event = $_POST['Event'];
$fielda = $_POST['fielda'];
$incident = $_POST['incident'];
$Name = $_POST['Name'];
$Alert = $_POST['Alert'];
$Source = $_POST['Source'];
$Who = $_POST['Who'];

echo "<br>"; 
echo "Values sent from datagrid" . "<br>";
echo "------------------------------------" . "<br>";
echo 'Time:  ' .$Time . "<br>";
echo 'Host:  ' .$Host . "<br>";
echo 'Monitor:  ' .$Monitor . "<br>";
echo 'Status:  ' .$Status . "<br>";
echo 'Event:  ' .$Event . "<br>";
echo 'User IP:  ' . $Who . "<br>";
echo "fielda: " . $fielda . "<br>";
echo "incident: " . $incident . "<br>";
echo "Name: " . $Name . "<br>";
echo "Alert: " . $Alert . "<br>";
echo "Source: " . $Source . "<br>";
echo "Who: " . $Who . "<br>";
echo '';
?>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script src="../js/datetimepicker_css.js"></script>
<script src="../js/MAdmin.js"></script>
</head>
<body onload="doOnLoad();">
	<br>
	<!-- the NewCssCal function is found in datetimepicker_css.js  -->
	Click the calendar icon:&nbsp;&nbsp;<img src="../images/cal.gif" onclick="javascript:NewCssCal('value1','yyyyMMdd','arrow',true,'24')" style="cursor:pointer"/>
	<br>to set date/time of acknowledgement: <input type="Text" id="value1" maxlength="25" size="25"/>&nbsp;
	<!-- the msg function is found in MAdmin.js and it calls acktoDB2.php  -->
	<INPUT TYPE="button" NAME="button" Value="Ack" onClick="msg(this.form)">
</body>
</html>