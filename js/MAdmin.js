/*
Name: Monitoring Admin
Date created: 2015-Jan-02 11:30
Creator: Dan Fischer
Copyright (c) 2015 Dan Fischer
FileName: MAdmin.js
Version: 0.1.0
*/
function msg(form){
	var Time = "<?php echo $Time;?>";
	var Host = "<?php echo $Host;?>";
	var Monitor = "<?php echo $Monitor;?>";
	var Status = "<?php echo $Status;?>";
	var Event = "<?php echo $Event;?>";
	var fielda = "<?php echo $fielda;?>";
	var incident = "<?php echo $incident;?>";
	var Name = "<?php echo $Name;?>";
	var Alert = "<?php echo $Alert;?>";
	var Source = "<?php echo $Source;?>";
	var curDateStr = document.getElementById("value1").value;
	post_to_url('../php/acktoDB2.php',{Time:Time,Host:Host,Monitor:Monitor,Status:Status,Event:Event,fielda:fielda,incident:incident,Name:Name,Alert:Alert,Source:Source,AckEnd:curDateStr});
}
function post_to_url(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
         }
    }
    document.body.appendChild(form);
    form.submit();
}
function selectHost () {
var hr = new XMLHttpRequest();  // Create our XMLHttpRequest object
var url = "php/runtool.php";     // Create some variables we need to send to our PHP file
var TestVar1 = document.getElementById("value1").value;
var TestVar2 = document.getElementById("value2").value;
var vars = "host="+TestVar1+"&tool="+TestVar2;
hr.open("POST", url, true);
hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
// Access the onreadystatechange event for the XMLHttpRequest object
hr.onreadystatechange = function() {
    if(hr.readyState == 4 && hr.status == 200) {
        var return_data = hr.responseText;
		var raw = return_data.replace(/(^\d+)/,"");
        document.getElementById("status").innerHTML = raw;
    }
}
// Send the data to PHP now... and wait for response to update the status div
hr.send(vars); // Actually execute the request
document.getElementById("status").innerHTML = "<br><br>processing...";
}
var postdata;  //used to get the data wanted by d3.xhr() below
function selectMetric (form) {
  var TestVar1 = form.value1.value;
  var TestVar2 = form.value2.value;
  var TestVar3 = form.value3.value;
  postdata = "data1=" + TestVar1 + "&data2=" + TestVar2 + "&data3=" + TestVar3;

  // Set the dimensions of the canvas/graph - made it smaller for testing
  var margin = {top: 25, right: 5, bottom: 50, left: 35},
  //width = 900 - margin.left - margin.right,
  //height = 400 - margin.top - margin.bottom;
  width = 370 - margin.left - margin.right,
  height = 175 - margin.top - margin.bottom;

  // Parse the date / time
  var parseDate = d3.time.format("%Y-%m-%d %H:%M:%S").parse;
  var formatTime = d3.time.format("%H:%M:%S");
  
  // Set the ranges
  var x = d3.time.scale().range([0, width]);
  var y = d3.scale.linear().range([height, 0]);

  // Define the axes
  var xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(5);
  var yAxis = d3.svg.axis().scale(y).orient("left").ticks(5);

  // Define the line
  var valueline = d3.svg.line()
  .x(function(d) { return x(d.tm); })
  .y(function(d) { return y(d.metric); });
	
  // Adds the svg canvas - remove old graph
  var svg = d3.select("#the_SVG_ID1").remove();
  svg = d3.select("body")
    .append("svg")
	.attr("id","the_SVG_ID1")  //give the svg a name so we can remove it before updating
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", 
    "translate(" + margin.left + "," + margin.top + ")");

function make_x_axis() {
return d3.svg.axis()
.scale(x)
.orient("bottom")
.ticks(5)
}
function make_y_axis() {
return d3.svg.axis()
.scale(y)
.orient("left")
.ticks(5)
}
	
  // Get the data - set header and pass parameters - receive and parse for display
  var TestVar4 = "Ping";
  var pingdata = postdata + "&data4=" + TestVar4;
  d3.xhr("php/metricdata.php")
    .header("Content-type", "application/x-www-form-urlencoded")
    .post(pingdata,function(error, rawData){
	var raw = rawData.responseText.replace(/(^\d+)/,"");
	//alert(raw);  //diagnostic - remove remark to see the clean data returned from database
    var data = JSON.parse(raw);
    data.forEach(function(d) {d.tm = parseDate(d.tm);d.metric = +d.metric;});
	
  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.tm; }));
  y.domain([0, d3.max(data, function(d) { return d.metric; })]);

  // Add the valueline path.
  svg.append("path").attr("class", "line").attr("d", valueline(data));

var div = d3.select("body").append("div")
.attr("class", "tooltip")
.style("opacity", 0);

 // Add dots to valueline path.
svg.selectAll("dot")
.data(data)
.enter().append("circle")
.filter(function(d) { return d.metric > 150 }) // <== apply red dots
.style("fill", "red")
.attr("r", 3)
.attr("cx", function(d) { return x(d.tm); })
.attr("cy", function(d) { return y(d.metric); })
.on("mouseover", function(d) {
div.transition()
.duration(200)
.style("opacity", .9);
div .html(formatTime(d.tm) + "<br/>" + d.metric)
.style("left", (d3.event.pageX) + "px")
.style("top", (d3.event.pageY - 28) + "px");
})
.on("mouseout", function(d) {
div.transition()
.duration(500)
.style("opacity", 0);
});

  // Add the X Axis and rotate text
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll("text")  
    .style("text-anchor", "end")
    .attr("dx", "-.75em")
    .attr("dy", ".12em")
    .attr("transform", function(d) {return "rotate(-90)" });

    // Add the Y Axis
    svg.append("g").attr("class", "y axis").call(yAxis);
	
//graph title
	svg.append("text")
.attr("x", (width / 2))
.attr("y", 0 - (margin.top / 2))
.attr("text-anchor", "middle")
.style("font-size", "16px")
.style("text-decoration", "underline")
.text("Ping");

//add gridlines
svg.append("g")
.attr("class", "grid")
.attr("transform", "translate(0," + height + ")")
.call(make_x_axis()
.tickSize(-height, 0, 0)
.tickFormat("")
)

svg.append("g")
.attr("class", "grid")
.call(make_y_axis()
.tickSize(-width, 0, 0)
.tickFormat("")
)
});
//================================================================================================
// Adds the svg canvas - remove old graph
  var chart2 = d3.select("#the_SVG_ID2").remove();
  chart2 = d3.select("body")
    .append("svg")
	.attr("id","the_SVG_ID2")  //give the svg a name so we can remove it before updating
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")"); 
  
// Get the data
var TestVar4 = "Mem";
var memdata = postdata + "&data4=" + TestVar4;
d3.xhr("php/metricdata.php")
    .header("Content-type", "application/x-www-form-urlencoded")
    .post(memdata,function(error, rawData){
	var raw = rawData.responseText.replace(/(^\d+)/,"");
    //alert(raw);  //diagnostic - remove remark to see the clean data returned from database
    var data = JSON.parse(raw);
    data.forEach(function(d) {d.tm = parseDate(d.tm);d.metric = +d.metric;});

// Scale the range of the data
x.domain(d3.extent(data, function(d) { return d.tm; }));
y.domain([0, d3.max(data, function(d) { return d.metric; })]);

// Add the valueline path.
chart2.append("path").attr("class", "line").attr("d", valueline(data));

// Add the X Axis and rotate text
  chart2.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll("text")  
    .style("text-anchor", "end")
    .attr("dx", "-.75em")
    .attr("dy", ".12em")
    .attr("transform", function(d) {return "rotate(-90)" });

// Add the Y Axis
chart2.append("g").attr("class", "y axis").call(yAxis);

//graph title
chart2.append("text")
.attr("x", (width / 2))
.attr("y", 0 - (margin.top / 2))
.attr("text-anchor", "middle")
.style("font-size", "16px")
.style("text-decoration", "underline")
.text("Memory");

//add gridlines
chart2.append("g")
.attr("class", "grid")
.attr("transform", "translate(0," + height + ")")
.call(make_x_axis()
.tickSize(-height, 0, 0)
.tickFormat("")
)

chart2.append("g")
.attr("class", "grid")
.call(make_y_axis()
.tickSize(-width, 0, 0)
.tickFormat("")
)
});
//================================================================================================
// Adds the svg canvas - remove old graph
  var chart3 = d3.select("#the_SVG_ID3").remove();
  chart3 = d3.select("body")
    .append("svg")
	.attr("id","the_SVG_ID3")  //give the svg a name so we can remove it before updating
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")"); 
  
// Get the data
var TestVar4 = "Disk";
var diskdata = postdata + "&data4=" + TestVar4;
d3.xhr("php/metricdata.php")
    .header("Content-type", "application/x-www-form-urlencoded")
    .post(diskdata,function(error, rawData){
	var raw = rawData.responseText.replace(/(^\d+)/,"");
    //alert(raw);  //diagnostic - remove remark to see the clean data returned from database
    var data = JSON.parse(raw);
    data.forEach(function(d) {d.tm = parseDate(d.tm);d.metric = +d.metric;});

// Scale the range of the data
x.domain(d3.extent(data, function(d) { return d.tm; }));
y.domain([0, d3.max(data, function(d) { return d.metric; })]);

// Add the valueline path.
chart3.append("path").attr("class", "line").attr("d", valueline(data));

// Add the X Axis and rotate text
  chart3.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll("text")  
    .style("text-anchor", "end")
    .attr("dx", "-.75em")
    .attr("dy", ".12em")
    .attr("transform", function(d) {return "rotate(-90)" });

// Add the Y Axis
chart3.append("g").attr("class", "y axis").call(yAxis);

//graph title
chart3.append("text")
.attr("x", (width / 2))
.attr("y", 0 - (margin.top / 2))
.attr("text-anchor", "middle")
.style("font-size", "16px")
.style("text-decoration", "underline")
.text("Disk");

//add gridlines
chart3.append("g")
.attr("class", "grid")
.attr("transform", "translate(0," + height + ")")
.call(make_x_axis()
.tickSize(-height, 0, 0)
.tickFormat("")
)

chart3.append("g")
.attr("class", "grid")
.call(make_y_axis()
.tickSize(-width, 0, 0)
.tickFormat("")
)
});

}