<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" -->
<head>
<!--
Name: Monitoring Admin
Date created: 2015-Jan-02 11:30
Creator: Dan Fischer
Copyright (c) 2015 Dan Fischer
FileName: stub.php
Version: 0.1.0
-->
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" type="text/css" href="css/MAmenu2.css" />
<!-- remarks -->
</head>
<body>
<div id="header">
	<div id="colortab" class="ddcolortabs">
		<ul>
		<li><a ><span>Monitoring-Admin</span></a></li>
		<li><a href="MAdmin.php" ><span>Back</span></a></li>
		</ul>
	</div>
</div>
<style>
body {
    margin:0px;
    color:#666;
    font-family: Tahoma, Arial, Helvetica, Tahoma, serif;
    font-size:20px;
    overflow-x:hidden;
}
</style>
<div class="ddcolortabsline">&nbsp;</div>
<br><br>
<center>This is a stub that can be used to add features</center>
<br>
<div id="myfooter">
	<p><strong>Footer</strong> (always at the bottom). "Good design is as little design as possible."</p>
</div>
<script type="text/javascript">tabdropdown.init("colortab", 0)</script>
</body>
</html>