<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" -->
<html>
<!--
Name: Monitoring Admin
Date created: 2015-Jan-02 11:30
Creator: Dan Fischer
Copyright (c) 2015 Dan Fischer
FileName: smart2.php
Version: 0.1.0
-->
<head>
<script type="text/javascript" src="js/MAdmin.js"></script> 
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="refresh" CONTENT="30; smart2.php">  
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title>SMART - the omnibus monitoring console</title>
<link href="css/table.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/dropdowntabs.js"></script>
<link rel="stylesheet" type="text/css" href="css/MAmenu3.css" />
<link rel="stylesheet" type="text/css" href="css/badges.css" />
</head>
<!-- remarks -->
<body>
<?php
require 'php/class.eyemysqladap.inc.php';
require 'php/class.eyedatagrid.inc.php';
$Who = getenv('REMOTE_ADDR');
?>
<script>var Who = "<?php echo $Who;?>";</script>
<?php

// Load the database adapter
include('php/dbconnect.php');
$db = new EyeMySQLAdap($dbhost, $dbuser, $dbpass, $database);
$x = new EyeDataGrid($db);
$x->setQuery("*", "smart_grid");
$x->hideColumn('fielda');
$x->hideColumn('incident');
$x->hideColumn('Name');
$x->hideColumn('Alert');
//$x->hideFooter();
//$x->showCheckboxes();
//$x->showRowNumber();
// Add standard control
$x->addStandardControl(EyeDataGrid::STDCTRL_ACK, "post_to_url('php/acktest3.php',{Time:'%Time%',Host:'%Host%',Monitor:'%Monitor%',Status:'%Status%',Event:'%Event%',fielda:'%fielda%',incident:'%incident%',Name:'%Name%',Alert:'%Alert%',Source:'%Source%',Who:Who})");
$x->addStandardControl(EyeDataGrid::STDCTRL_DELETE, "alert('Deleting or something else')");
$x->addStandardControl(EyeDataGrid::STDCTRL_EDIT, "post_to_url('php/tkt.php',{Time:'%Time%',Host:'%Host%',Monitor:'%Monitor%',Status:'%Status%',Event:'%Event%',fielda:'%fielda%',incident:'%incident%',Name:'%Name%',Alert:'%Alert%',Source:'%Source%',Who:Who})");
//$x->addRowSelect("alert('You have selected a row id ')");
if (EyeDataGrid::isAjaxUsed())
{
	$x->printTable();
	exit;
}
?>
<div id="header">
	<div id="colortab" class="ddcolortabs">
		<ul>
		<li><a ><span>Monitoring-Admin</span></a></li>
		<li><a href="MAdmin.php" ><span>Back</span></a></li>
		</ul>
	</div>
</div>
<div class="ddcolortabsline">&nbsp;</div>
<div class="somecontent" style=margin:3px;color:black;top:31px;position:fixed;width:100%;height:auto;>
	<center>
		<?php
			$x->printTable();
		?>
	</center>
	<center><!-- have not decided how the badges will be used - currently these are optional -->
		<div id="api_info_div" style="height:24px; margin-top: 5px"
			<div class="row">
				<span class="myBadge1" style="margin-right: 5px;">Read 1</span>
				<span class="myBadge2" style="margin-right: 5px;">Read 2</span>
				<span class="myBadge3" style="margin-right: 5px;">Read 3</span>
				<span class="myBadge4" style="margin-right: 5px;">Read 4</span>
			</div>
		</div>
	</center>
</div>
<div id="myfooter">
	<p>
		<strong>Footer</strong> (always at the bottom). "Good design is as little design as possible."
	</p>
</div>
</body>
<script type="text/javascript">tabdropdown.init("colortab", 0)</script>
</html>
