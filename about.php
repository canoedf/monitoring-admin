<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" -->
<head>
<!--
Name: Monitoring Admin
Date created: 2015-Jan-02 11:30
Creator: Dan Fischer
Copyright (c) 2015 Dan Fischer
FileName: about.php
Version: 0.1.0
-->
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" type="text/css" href="css/MAmenu2.css" />
<!-- remarks -->
</head>
<body>
<div id="header">
	<div id="colortab" class="ddcolortabs">
		<ul>
		<li><a ><span>Monitoring-Admin</span></a></li>
		<li><a href="MAdmin.php" ><span>Back</span></a></li>
		</ul>
	</div>
</div>
<style>
body {
    margin:0px;
    color:#666;
    font-family: Tahoma, Arial, Helvetica, Tahoma, serif;
    font-size:18px;
    overflow-x:hidden;
	top: 25px;
	position:fixed;
	width: auto;
	height: auto;
}
</style>
<div class="ddcolortabsline">&nbsp;</div>
<div id="about"  style=position:relative;left:5px;>
<?php
echo "<br><br>";
echo 'This MONITORING-ADMIN operations tool, meant for internal workstation usage, was originally conceived and developed' . "<br>";
echo 'in response to the awful monitoring tool user interfaces I had to endure.' . "<br>";
echo "<br>";
echo 'The original effort was confined to a status display that I called SMARTS showing error/warning detail provided by the monitoring alert function.  Now I have included a performance display and some tools.' . "<br>";
echo "<br>";
echo 'The tool does not rely on any UI "framework".  Current frameworks do not provide enough support for all the integrations that may grow with this tool.' . "<br>";
echo "Dan" . "<br>";
echo "<br>";
echo "The tool currently relies on these: <br>";
echo "/**************************************************************************<br>";
echo "* http://www.rainforestnet.com/datetimepicker/datetimepicker.htm<br>";
echo "* Copyright (c) 2010-2014, Teng-Yong Ng . All rights reserved.<br>";
echo "* Drop Down Tabs Menu script- © Dynamic Drive (http://www.dynamicdrive.com)<br>";
echo "* D3 Javascript project:  http://d3js.org/<br>";
echo "* and the defunct Eyesis project<br>";
echo "* @author     Micheal Frank <mike@eyesis.ca><br>";
echo "* @copyright  2008 Eyesis @version    v1.0.1 11/26/2008 9:41:46 AM<br>";
echo "* @license    http://www.eyesis.ca/license.txt  BSD License<br>";
echo "/**************************************************************************<br>";
?>
</div>
<div id="myfooter">
	<p><strong>Footer</strong> (always at the bottom). "Good design is as little design as possible."</p>
</div>
<script type="text/javascript">tabdropdown.init("colortab", 0)</script>
</body>
</html>